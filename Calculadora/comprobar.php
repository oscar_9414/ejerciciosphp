<?php
    $correcto=0;
    if (isset($_GET['enviar1'])) {//esto lo uso para poder saber cual de los dos submit tengo recibir para cada operacio
        if (isset($_GET['num1']) && !empty($_GET['num1'])) {
        $num1 = $_GET['num1'];//si existe me gaurdo en num1 el valor que mando del formulario

    } else {
        $correcto = 1;//valdrá 1 si no se introdujeron datos o la variable no existe
        // echo "error";
    }
    if (isset($_GET['num2']) && !empty($_GET['num2'])) {
        $num2 = $_GET['num2'];//si existe me gaurdo en num2 el valor que mando del formulario
    } else {
        $correcto = 1;//valdrá 1 si no se introdujeron datos o la variable no existe
        // echo "error";
    }

    if ($correcto == 0) {//si num1 ó num2 tienen algún valor entrará
        if (is_numeric($num1) && is_numeric($num2)) {//compruebo que los datos introducidos sean numeros
            if ($_GET['operador'] == "+") {
                echo "Has elegido hacer una suma: <br>";
                $solucion = $num1 + $num2;
                echo  $num1." + ".$num2." = ".$solucion;
                echo '<p><a href="calculadora.php">Volver a calcular</a></p>';
            }
            if ($_GET['operador'] == "-") {
                echo "<br>Has elegido hacer una resta<br>";
                $solucion = $num1 - $num2;
                echo $num1." - ".$num2." = ".$solucion;
                echo '<p><a href="calculadora.php">Volver a calcular</a></p>';
            }
            if ($_GET['operador'] == "x") {
                echo "<br>Has elegido hacer una multiplicación<br>";
                $solucion = $num1 * $num2;
                echo $num1." x ".$num2." = ".$solucion;
                echo '<p><a href="calculadora.php">Volver a calcular</a></p>';
            }
            if ($_GET['operador'] == "÷") {
                echo "<br>Has elegido hacer una divición<br>";
                $solucion = $num1 / $num2;
                echo $num1." ÷ ".$num2." = ".$solucion;
                echo '<p><a href="calculadora.php">Volver a calcular</a></p>';

            }
        }
        // else {//else if interno
        //     echo "Por favor introduce datos correctos (numeros)";
        //     echo '<p><a href="calculadora.php">Volver a calcular</a></p>';
        // }
    } else {
        echo "<h3>Por favor RELLENA LOS DOS CAMPOS</h3>";
        echo '<p><a href="calculadora.php">Volver a calcular</a></p>';
    }

    }

if (isset($_GET['enviar2'])) {//esto lo uso para poder saber cual de los dos submit tengo recibir para cada operacio
    $elevarCorrecto=0;
    if (isset($_GET['num3']) && !empty($_GET['num3'])) {
        $num3 = $_GET['num3'];//num 3 será la base
    } else {
        $elevarCorrecto = 1;
    }
    if (isset($_GET['num4']) && !empty($_GET['num4'])) {
        $num4 = $_GET['num4'];//num 4 el exponente
    } else {
        $elevarCorrecto = 1;
    }

    if ($elevarCorrecto == 0) {//solo pasará a esta parte del if si existe la variable y tiene datos
        $resultado = 0;
        echo "<h3>Has elejido elevar un numero</h3>";
            $resultado = pow($num3, $num4);
        echo $num3. " elevado a: ".$num4." = ".$resultado;
        echo '<p><a href="calculadora.php">Volver a calcular</a></p>';
    } else {//de lo contrario pasara a pedirte de nuevo los datos
        echo "Rellena los campos con numeros";
        echo '<p><a href="calculadora.php">Volver a calcular</a></p>';
    }
}
