<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 3</title>
</head>
<body>
    <?php
        $array = array(
            "Lunes","Martes","Miercóles","Jueves","Viernes","Sábado","Domingo",);
        echo "<b>Prueba con for normal<br>";
        echo "<table border='1'><tr>";
        for ($i = 0 ; $i < 7; $i++) {
                echo "<th>$array[$i]</th>";
            }
        echo "</tr></table>";
        echo "</b>";
        echo "<ul>";
         for ($i = 0 ; $i < 7; $i++) {
                echo "<li>$array[$i]</li>";
        }
        echo "</ul>";
        echo "<b>Prueba con foreach<br>";
        echo "<table border='1'><tr>";
        foreach ($array as $dia) {
             echo "<th>$dia</th>";
        }


        echo "</tr></table>";
        echo "</b>";
        echo "<ul>";
        foreach ($array as $dia) {
             echo "<li>$dia</li>";
        }
        echo "</ul>";
        ?>

</body>
</html>
