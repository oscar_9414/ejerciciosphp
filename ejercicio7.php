<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 7</title>
</head>
<body>
    <?php
        if (isset($_REQUEST['jugadores'])) {
            $jugadores = $_REQUEST['jugadores'];
        }else{
            $jugadores =[];
            //$jugadores=array{};
        }
        if (isset($_REQUEST['nuevo'])){
            $jugadores[]= $_REQUEST['nuevo'];//lo añado al final
        }
     ?>
     <h1>Lista de jugadores</h1>
     <form>
         nuevo: <input type="text" name="nuevo"><br>
         <input type="submit" name="enviar">
     </form>
     <ul>
        <?php foreach ($jugadores as $jugador):?>
            <li><?php echo "$jugador";?></li>
        <?php endforeach ?>
     </ul>
     <h3>Nuevo jugador</h3>
     <form>
         Nombre nuevo: <input type="text" name="nuevo">
         <br>
         <input type="submit" name="enviar">
         <br>
         <?php foreach ($jugadores as $key => $jugador):?>
            <input type="hidden" name="jugadores[]" value="<?php echo $jugador ?>">

        <?php endforeach ?>
     </form>
</body>
</html>
<!--## Ejercicio7.php
- En el ejercicio 7 vammos a usar un sólo fichero _ejercicio7.php_
- El formulario va a enviar los datos a si mismo.
- Vamos a formar una lista de jugadores de futbol, cada vez que enviemos guardaremos un nuevo elemento en la lista.
- PISTA: usa control(es) de tipo _hidden_.-->
