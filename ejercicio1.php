<!DOCTYPE html>
<html>
<head>
    <title>ejercicio 01</title>
</head>
<body>
    <?php
        $nombre = "Oscar";
        $apellido = "Portillo";
        $edad = 24;
        echo '<strong>Probando las comillas simples:</strong><br>';
        echo 'Informacion del Alumno <br>';
        echo '<ul><li>Nombre: $nombre</li>';
        echo '<li>Apellido: $apellido</li>';
        echo '<li>Edad: $edad</li></ul>';
        echo "<p><strong>Probando las comillas dobles:</strong><br>";
        echo "Informacion del Alumno <br>";
        echo "<ul><li>Nombre: $nombre</li>";
        echo "<li>Apellido: $apellido</li>";
        echo "<li>Edad: $edad</li></ul></p>";
             ?>

</body>
</html>
