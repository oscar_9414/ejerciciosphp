<?php

    /**
    *
    */
    class AppApuesta
    {

        function __construct()
        {
            session_start();
        }//consttructor
        public function inicio(){
            require('vistaTabla.php');
        }
        public function toggle(){
            $apuesta = $_SESSION['apuesta'];
            $numero = $_GET['numero'];
            foreach ($apuesta as $numeroApostado) {
                if ($numeroApostado == $numero) {
                    unset($apuesta["$numero"]);
                    $_SESSION['apuesta'] = $apuesta;
                    header("Location:?method=inicio");
                }//if
            }//for
            header("Location:?method=inicio");
            $apuesta[$numero] = $numero;
            $_SESSION['apuesta'] = $apuesta;
           // echo "<br><a href='?method=inicio'>volver a apostar</a>";
        }//añadirQuitar metodo
        public function apostar(){
            if (count($_SESSION['apuesta']) < 6 ) {
                $_SESSION['mensaje'] ="<h1>Apuesta Incompleta</h2>";
                $_SESSION['valida'] = false;
            } elseif ( count($_SESSION['apuesta'])== 6 ) {
                $_SESSION['mensaje'] = "Apuesta simple";
                $_SESSION['valida'] = true;
            } else {
                $_SESSION['mensaje'] = "apuesta multiple";
                $_SESSION['valida'] = true;
            }
            $this->validarApuesta();
        }
        public function validarApuesta(){

                require('comprobar.php');

        }
        public function reiniciar(){
            unset($_SESSION["apuesta"]);
            unset($_SESSION["mensaje"]);
            unset($_SESSION["valido"]);

            require "vistaTabla.php";
        }
        public function contadorNumeros(){
            $_SESSION['contador'] =" <h3>LLevas :".count($_SESSION['apuesta'])." Numeros apostados</h3>";
        }
    }//cass
